# Copyright (c) 2019-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

---

# The public bits of the keys GitLab signs their APT packages with.
gitlab_runner_apt_keys:
  gitlab:
    data: "{{ lookup('file', 'gitlab_runner.gpg') }}"
    state: "present"

# The path to the GitLab APT source list file on Debian systems.
gitlab_runner_apt_repository_file:
  "/etc/apt/sources.list.d/gitlab_runner.list"

gitlab_runner_release:
  "{{ custom_gitlab_runner_release
    | default(ansible_distribution_release) }}"

# The content of the GitLab APT source list file on Debian systems.
gitlab_runner_apt_repository_record: >-
  deb https://packages.gitlab.com/runner/gitlab-runner/{{ ansible_distribution | lower }}/
  {{ gitlab_runner_release }} main

  deb-src https://packages.gitlab.com/runner/gitlab-runner/{{ ansible_distribution | lower }}/
  {{ gitlab_runner_release }} main

# Whether automatic cache clearing is active
gitlab_runner_clear_cache:
  true

# Number of days the cached files should be present
gitlab_runner_clear_cache_threshold:
  "7"

# The name of the Docker package.
gitlab_runner_package_name:
  "gitlab-runner"

# The version of the gitlab-runner package.
gitlab_runner_package_version:
  "16.8.1"

# The target state of the Docker package.
gitlab_runner_package_state:
  "present"

# Whether to enable the GitLab service to start during system boot.
gitlab_runner_service_enabled:
  true

# The actual name of the GitLab service.
gitlab_runner_service_name:
  "gitlab-runner"

# The target state of the service; either "started" or "stopped".
gitlab_runner_service_state:
  "{{ gitlab_runner_service_enabled | ternary('started', 'stopped') }}"

# Whether to "reload" or "restart" the service after configuration updates.
gitlab_runner_service_strategy:
  "restart"

# The timing of the cleanup cron jobs
# Sundays 02:00
gitlab_runner_cron_job_weekday:
  "0"
gitlab_runner_cron_job_hour:
  "2"
gitlab_runner_cron_job_minute:
  "0"

# Prerequisite packages to install gitlab-runner
gitlab_runner_prerequisite_packages:
  "{{ gitlab_runner_default_prerequisite_packages }}"
