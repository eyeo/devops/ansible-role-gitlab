# Copyright (c) 2018-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

---

# https://docs.ansible.com/ansible/latest/modules/apt_module.html
- name:
    "apt : prerequisites"
  apt:
    cache_valid_time: "{{ apt_cache_valid_time | default(omit) }}"
    default_release: "{{ apt_default_release | default(omit) }}"
    name: "{{ gitlab_runner_prerequisite_packages }}"
    update_cache: "{{ apt_update_cache | default(omit) }}"
  become:
    true

# https://docs.ansible.com/ansible/latest/modules/apt_key_module.html
- apt_key:
    data: "{{ item.value.data | default(omit) }}"
    file: "{{ item.value.file | default(omit) }}"
    id: "{{ item.value.id | default(omit) }}"
    keyring: "/etc/apt/trusted.gpg.d/\
      {{ item.value.keyring | default('gitlab-runner') }}\
      .gpg"
    state: "{{ item.value.state | default(omit) }}"
    url: "{{ item.value.url | default(omit) }}"
  loop:
    "{{ gitlab_runner_apt_keys | dict2items }}"
  loop_control:
    label: "{{ [item.key, item.value.state | default('present')] | to_json }}"
  register:
    "gitlab_runner_apt_keys"
  become:
    true

# Note that, in contrast to the more commonly used `apt_repository` module,
# the use of module `copy` allows for easier idempotence.
# https://docs.ansible.com/ansible/latest/modules/apt_repository_module.html
# https://docs.ansible.com/ansible/latest/modules/copy_module.html
- name:
    "apt_repository"
  copy:
    content: "{{ gitlab_runner_apt_repository_record }}"
    dest: "{{ gitlab_runner_apt_repository_file }}"
    mode: "0644"
  register:
    "gitlab_runner_apt_repository"
  become:
    true

# https://docs.ansible.com/ansible/latest/modules/apt_module.html
- apt:
    cache_valid_time: "{{ gitlab_runner_apt_cache_valid_time }}"
    default_release: "{{ apt_default_release | default(omit) }}"
    name: "{{ gitlab_runner_package_name }}={{ gitlab_runner_package_version }}"
    state: "{{ gitlab_runner_package_state }}"
    update_cache: "{{ gitlab_runner_apt_update_cache }}"
  vars:
    gitlab_runner_apt_cache_valid_time:
      "{{ gitlab_runner_apt_repository.changed or
          gitlab_runner_apt_keys.changed
        | ternary(0, apt_cache_valid_time | default(omit)) }}"
    gitlab_runner_apt_update_cache:
      "{{ gitlab_runner_apt_repository.changed or
          gitlab_runner_apt_keys.changed
        | ternary(true, apt_update_cache | default(omit)) }}"
  become:
    true

# https://gitlab.com/gitlab-org/gitlab-runner/issues/4449
# https://gitlab.com/gitlab-org/gitlab-runner/issues/3849
# https://docs.ansible.com/ansible/latest/modules/file_module.html
- name:
    "file : ~gitlab-runner/.bash_logout : absent"
  file:
    path: "/home/gitlab-runner/.bash_logout"
    state: "absent"
  become:
    true
