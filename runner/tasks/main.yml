# Copyright (c) 2018-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

---

# https://docs.ansible.com/ansible/latest/modules/include_vars_module.html
- include_vars:
    "{{ gitlab_runner_vars }}"
  with_first_found:
    # yamllint disable rule:line-length
    - "vars/{{ ansible_os_family | lower }}/{{ ansible_distribution_release | lower }}.yml"
    # yamllint enable rule:line-length
    - "vars/{{ ansible_os_family | lower }}.yml"
    - "vars/debian.yml"
  loop_control:
    loop_var: "gitlab_runner_vars"

# https://docs.ansible.com/ansible/latest/modules/include_tasks_module.html
- include_tasks:
    "{{ gitlab_runner_tasks }}"
  with_first_found:
    - "os/{{ ansible_os_family | lower }}/main.yml"
    - "os/generic/main.yml"
  loop_control:
    loop_var: "gitlab_runner_tasks"

# https://docs.ansible.com/ansible/latest/modules/service_module.html
- service:
    enabled: "{{ gitlab_runner_service_enabled }}"
    name: "{{ gitlab_runner_service_name }}"
    state: "{{ gitlab_runner_service_state }}"
  become:
    true

# Documentation:
# $ ANSIBLE_LIBRARY=./library ansible-doc -t module gitlab_runner
- name:
    "gitlab_runner : all runners"
  gitlab_runner:
    parameters: "{{ item['parameters'] | mandatory }}"
    flags: "{{ item['flags'] | default([]) }}"
    state: "{{ item['state'] | default(omit) }}"
    protected: "{{ item['protected'] | default(omit) }}"
    personal_access_token: "{{ item['personal_access_token'] | default(omit) }}"
  loop:
    "{{ gitlab_runners | default([]) }}"
  loop_control:
    label: >-
      {{ item['parameters']['name'] }},
      {{ item['state'] | default('present') }}
  become:
    true

# https://docs.gitlab.com/runner/configuration/advanced-configuration.html
- name:
    "replace : config.toml : concurrent"
  replace:
    path: "/etc/gitlab-runner/config.toml"
    regexp: "^\\s*(#\\s*)?concurrent\\s*=.*"
    replace: "concurrent = {{ gitlab_runner_concurrent | default(2) }}"
    backup: "no"
  notify:
    - "service : gitlab-runner"
  become:
    true

# https://docs.ansible.com/ansible/latest/modules/template_module.html
- name:
    "template : list-gitlab-runner-directories"
  template:
    src: "list-gitlab-runner-directories.py.j2"
    dest: "/usr/local/sbin/list-gitlab-runner-directories"
    mode: "0700"
  register:
    "list_gitlab_runner_directories"
  become:
    true

# https://docs.ansible.com/ansible/latest/modules/template_module.html
- name:
    "template : clear-shell-runner-caches"
  template:
    src: "clear-shell-runner-caches.j2"
    dest: "/usr/local/sbin/clear-shell-runner-caches"
    mode: "0700"
  register:
    "clear_shell_runners_cache"
  become:
    true

# https://docs.ansible.com/ansible/latest/modules/cron_module.html
- name:
    "cron : clear-shell-runner-caches"
  cron:
    weekday: "{{ gitlab_runner_cron_job_weekday }}"
    hour: "{{ gitlab_runner_cron_job_hour }}"
    minute: "{{ gitlab_runner_cron_job_minute }}"
    user: "root"
    name: "Automatically clean up outdated local gitlab-runner caches."
    job: "{{ clear_shell_runners_cache.dest }}"
    state: "{{ gitlab_runner_clear_cache | ternary('present', 'absent') }}"
  become:
    true

# https://docs.gitlab.com/runner/executors/docker.html#clearing-docker-cache
# https://docs.ansible.com/ansible/latest/modules/cron_module.html
- name:
    "cron : clear-docker-cache"
  cron:
    weekday: "{{ gitlab_runner_cron_job_weekday }}"
    hour: "{{ gitlab_runner_cron_job_hour }}"
    minute: "{{ gitlab_runner_cron_job_minute }}"
    user: "gitlab-runner"
    name: "Automatically clean up outdated docker-gitlab-runner caches."
    job: "/usr/share/gitlab-runner/clear-docker-cache"
    state: "{{ gitlab_runner_clear_cache | ternary('present', 'absent') }}"
  become:
    true
